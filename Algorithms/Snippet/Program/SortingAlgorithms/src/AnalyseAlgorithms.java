
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class AnalyseAlgorithms {
	private static final String UNDERSCORE = "_";
	private static final String FILE_NAME_PREFIX = "DataSet_";
	private static final String FILE_EXTENSION = ".txt";
	private static final String NEW_LINE = "\r\n";
	private static final long _1000 = 1000L;
	private static final long _5000 = 5000L;
	private static final long _50000 = 50000L;
	private static File outputFileName = new File("output.txt");
	private static FileWriter outputFileWriter = null;
	private static final String FOLDER_NAME = "DataSets\\";

	public static void main(String[] args) {
		AnalyseAlgorithms analyser = new AnalyseAlgorithms();

		// Error Message is showed if No arguments passed
		if (args.length == 0) {
			System.out.println(
					"Error!! Input required. 'D' to create the data set OR 'S' to produce the sorting results");
			return;
		}
		String inputArg = args[0];
		switch (inputArg) {
		// Creating 10 Data Sets each for 1000, 5000 and 50000 when argument is 'D'
		case "D":
			File dir = new File(FOLDER_NAME);
			dir.mkdir();
			for (int i = 1; i <= 10; i++) {
				createDataSet(_1000, i);
				createDataSet(_5000, i);
				createDataSet(_50000, i);
			}
			System.out.println("DataSets created successfully!!");
			break;
		// Running The Algorithms for all the data sets when argument is 'S'
		case "S":
			try {
				outputFileWriter = new FileWriter(FOLDER_NAME + outputFileName);
				for (int i = 1; i <= 10; i++) {
					analyser.sort("I", _1000, i);
					analyser.sort("H", _1000, i);
					analyser.sort("Q", _1000, i);
				}
				for (int i = 1; i <= 10; i++) {
					analyser.sort("I", _5000, i);
					analyser.sort("H", _5000, i);
					analyser.sort("Q", _5000, i);
				}
				for (int i = 1; i <= 10; i++) {
					analyser.sort("I", _50000, i);
					analyser.sort("H", _50000, i);
					analyser.sort("Q", _50000, i);
				}

			} catch (IOException e1) {
				System.out.println("IOException has occured!!");
				break;
			} finally {
				try {
					outputFileWriter.close();
				} catch (IOException e) {
					return;
				}
			}
			System.out.println("Output file created as required!!");
			break;
		default:
			System.out.println("Input not identified !!");
			break;
		}
	}

	/**
	 * Function to create the data set. It populates the data elements in different
	 * files with the name convention of "DataSet_{DataSize}_{DataSet}.txt" example:
	 * DataSet_1000_1.txt, DataSet_5000_10.txt
	 */
	private static void createDataSet(final long dataSize, final int setNumber) {
		Random random = new Random();
		File file = new File(FOLDER_NAME + FILE_NAME_PREFIX + dataSize + UNDERSCORE + setNumber + FILE_EXTENSION);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file);
			Set<Integer> numbers = new HashSet<Integer>();
			while (numbers.size() < dataSize) {
				int integer = random.nextInt(1000000);
				if (!numbers.contains(integer)) {
					numbers.add(integer);
				}
			}
			for (Integer integer : numbers) {
				fileWriter.append(String.valueOf(integer));
				fileWriter.append(NEW_LINE);
			}
		} catch (Exception e) {

		} finally {
			try {
				if (fileWriter != null) {
					fileWriter.close();
				}

			} catch (IOException e) {
				System.out.println("IOException!!");
			}
		}
	}

	/**
	 * Calls the Sorting algorithms based on the passed arguments of sortMethod for
	 * the particular data set. This function also calculates the CPU time and the
	 * theoritical Comparisons for each algorithm
	 */
	private void sort(final String sortMethod, long dataSize, int setNumber) throws NumberFormatException, IOException {
		int[] array = getArrayFromFile(dataSize, setNumber);
		long start = 0L;
		long end = 0L;
		int numOfComparisons = 0;
		double theoreticalComparisons = 0D;
		switch (sortMethod) {
		// Calling Insertion Sort
		case "I":
			InsertionSort insertionSort = new InsertionSort();
			start = System.nanoTime();
			numOfComparisons = insertionSort.sort(array);
			end = System.nanoTime();
			theoreticalComparisons = ((dataSize * (dataSize - 1)) / 2);
			break;
		// Calling Heap sort
		case "H":
			HeapSort heapSort = new HeapSort();
			start = System.nanoTime();
			numOfComparisons = heapSort.sort(array);
			end = System.nanoTime();
			theoreticalComparisons = (2 * dataSize * (double) (Math.log(dataSize) / Math.log(2)));
			break;
		// Calling Quick Sort
		case "Q":
			QuickSort quickSort = new QuickSort();
			start = System.nanoTime();
			numOfComparisons = quickSort.sort(array, 0, array.length - 1, 0);
			end = System.nanoTime();
			theoreticalComparisons = (dataSize * (dataSize - 1)) / 2;
			break;

		default:
			break;
		}
		// Calculation of CPU time
		Long cpuTime = end - start;
		String doubleValue = String.format("%.6f", cpuTime.doubleValue() / 1000000000);

		// Creating output result as per the expected result
		final OutputData data = new OutputData.OutputDataBuilder().sortMethod(sortMethod).setNumber(setNumber)
				.dataSize(dataSize).numOfComparisons(numOfComparisons).cpuTime(doubleValue)
				.theoreticalComparisons(theoreticalComparisons).build();
		this.outputToFile(data);
	}

	/**
	 * Get the array of elements from the files for the given data size and set
	 * number
	 */
	private static int[] getArrayFromFile(final long dataSize, final int setNumber)
			throws NumberFormatException, IOException {
		@SuppressWarnings("resource")
		final BufferedReader br = new BufferedReader(
				new FileReader(FOLDER_NAME + FILE_NAME_PREFIX + dataSize + UNDERSCORE + setNumber + FILE_EXTENSION));
		String line;
		List<Float> listOfIntegers = new ArrayList<>();
		while ((line = br.readLine()) != null) {
			String[] data = line.split(",");
			for (String string : data) {
				listOfIntegers.add(Float.parseFloat(string));
			}
		}
		int[] array = listOfIntegers.stream().mapToInt(i -> i.intValue()).toArray();
		return array;
	}

	/**
	 * Appending result set to 'Output.txt'
	 */
	private void outputToFile(final OutputData data) throws IOException {
		outputFileWriter.append(data.toString());
		outputFileWriter.append(NEW_LINE);
	}

}
