/**
 * Implementation for Quick Sorting Algorithm
 */
public class QuickSort {

	private static int comparisons = 0;

	protected int sort(int[] array, int lowerIndex, int higherIndex, int actualComparisons) {
		comparisons = actualComparisons;
		int i = lowerIndex;
		int j = higherIndex;
		partition(array, lowerIndex, higherIndex, i, j);
		return comparisons;
	}

	private void partition(int[] array, int lowerIndex, int higherIndex, int i, int j) {
		// Calculate the pivot value from the middle
		int pivot = array[lowerIndex + (higherIndex - lowerIndex) / 2];
		// Divide the array into two
		while (i <= j) {
			comparisons++;
			while (array[i] < pivot) {
				i++;
			}
			comparisons++;
			while (array[j] > pivot) {
				j--;
			}
			if (i <= j) {
				swap(array, i, j);
				i++;
				j--;
			}
		}
		// Call quick sort recursively
		if (lowerIndex < j) {
			sort(array, lowerIndex, j, comparisons);
		}
		if (i < higherIndex) {
			sort(array, i, higherIndex, comparisons);
		}
	}

	// Exchange the numbers
	private void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}