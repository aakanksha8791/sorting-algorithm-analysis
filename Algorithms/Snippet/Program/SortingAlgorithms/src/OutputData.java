/**
 * Output Result for the algorithms are populated in this object using Builder
 * Pattern
 */
public class OutputData {

	private String sortMethod;
	private String setNumber;
	private String dataSize;
	private String numOfComparisons;
	private String cpuTime;
	private String theoreticalComparisons;

	public OutputData(OutputDataBuilder builder) {
		this.sortMethod = builder.sortMethod;
		this.setNumber = builder.setNumber;
		this.dataSize = builder.dataSize;
		this.numOfComparisons = builder.numOfComparisons;
		this.cpuTime = builder.cpuTime;
		this.theoreticalComparisons = builder.theoreticalComparisons;
	}

	public String getSortMethod() {
		return sortMethod;
	}

	public String getSetNumber() {
		return setNumber;
	}

	public String getDataSize() {
		return dataSize;
	}

	public String getNumOfComparisons() {
		return numOfComparisons;
	}

	public String getCpuTime() {
		return cpuTime;
	}

	public String getTheoreticalComparisons() {
		return theoreticalComparisons;
	}

	public static class OutputDataBuilder {

		private String sortMethod;
		private String setNumber;
		private String dataSize;
		private String numOfComparisons;
		private String cpuTime;
		private String theoreticalComparisons;

		public OutputDataBuilder sortMethod(final String sortMethod) {
			this.sortMethod = sortMethod;
			return this;
		}

		public OutputDataBuilder setNumber(final int setNumber) {
			this.setNumber = String.valueOf(setNumber);
			return this;
		}

		public OutputDataBuilder dataSize(final long dataSize2) {
			this.dataSize = String.valueOf(dataSize2);
			return this;
		}

		public OutputDataBuilder numOfComparisons(final int numOfComparisons) {
			this.numOfComparisons = String.valueOf(numOfComparisons);
			return this;
		}

		public OutputDataBuilder cpuTime(final String format) {
			this.cpuTime = format;
			return this;
		}

		public OutputDataBuilder theoreticalComparisons(double theoreticalComparisons) {
			this.theoreticalComparisons = String.format("%.2f", theoreticalComparisons);
			return this;
		}

		OutputData build() {
			return new OutputData(this);
		}
	}

	@Override
	public String toString() {
		return new String(this.sortMethod + "," + this.setNumber + "," + this.dataSize + "," + this.numOfComparisons
				+ "," + this.cpuTime + "," + this.theoreticalComparisons);
	}
}
