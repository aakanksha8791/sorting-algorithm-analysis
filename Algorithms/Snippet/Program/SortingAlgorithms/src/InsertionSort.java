/**
 * Implementation for Insertion Sorting Algorithm
 */
public class InsertionSort {

	protected int sort(int[] array) {
		int comparisons = 0;
		for (int i = 0; i < array.length; i++) {
			int key = array[i];
			int j = i - 1;
			while (j >= 0 && key < array[j]) {
				comparisons++;
				array[j + 1] = array[j];
				j--;
			}
			array[j + 1] = key;
		}
		return comparisons;
	}
}
