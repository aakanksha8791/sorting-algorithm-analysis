/**
 * Implementation for Heap Sorting Algorithm
 */
public class HeapSort {
	private static int arraySize;
	private static int comparisons;

	public int sort(int[] array) {
		comparisons = 0;
		arraySize = array.length;
		// Building heap
		for (int i = arraySize / 2 - 1; i >= 0; i--) {
			buildMaxHeap(array, arraySize, i);
		}

		// Extracting the elements in the heap
		for (int i = arraySize - 1; i >= 0; i--) {
			// Move the root to the end
			swap(array, 0, i);
			// Re build the heap on the reduced heap
			buildMaxHeap(array, i, 0);
		}
		return comparisons;
	}

	public static void buildMaxHeap(int arr[], int arraySize, int i) {
		int left = (2 * i) + 1;
		int right = (2 * i) + 2;
		int max = i;

		if (left < arraySize && arr[left] > arr[max]) {
			max = left;
			comparisons++;
		} else if (left < arraySize) {
			comparisons++;
		}
		if (right < arraySize && arr[right] > arr[max]) {
			max = right;
			comparisons++;
		} else if (right < arraySize) {
			comparisons++;
		}
		// Make the root the largest
		if (max != i) {
			swap(arr, i, max);
			// Re build heap on the sub tree
			buildMaxHeap(arr, arraySize, max);
		}
	}

	// Swap the numbers
	public static void swap(int array[], int i, int j) {
		int tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;
	}

}