
//Main Application Class: AnalyseAlgorithms.java
//AnalyseAlgorithms gives a call to the 3 sorting classes i.e. //InsertionSort.java, HeapSort.java, QuickSort.java
//Created a common OutputData.java which contains the parameters //to populate the output resultset

//AnalyseAlgorithms.java

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class AnalyseAlgorithms {
	private static final String UNDERSCORE = "_";
	private static final String FILE_NAME_PREFIX = "DataSet_";
	private static final String FILE_EXTENSION = ".txt";
	private static final String NEW_LINE = "\r\n";
	private static final long _1000 = 1000L;
	private static final long _5000 = 5000L;
	private static final long _50000 = 50000L;
	private static File outputFileName = new File("output.txt");
	private static FileWriter outputFileWriter = null;
	private static final String FOLDER_NAME = "DataSets\\";

	public static void main(String[] args) {
		AnalyseAlgorithms analyser = new AnalyseAlgorithms();

		// Error Message is showed if No arguments passed
		if (args.length == 0) {
			System.out.println(
					"Error!! Input required. 'D' to create the data set OR 'S' to produce the sorting results");
			return;
		}
		String inputArg = args[0];
		switch (inputArg) {
		// Creating 10 Data Sets each for 1000, 5000 and 50000 when argument is 'D'
		case "D":
			File dir = new File(FOLDER_NAME);
			dir.mkdir();
			for (int i = 1; i <= 10; i++) {
				createDataSet(1000L, i);
				createDataSet(5000L, i);
				createDataSet(50000L, i);
			}
			System.out.println("DataSets created successfully!!");
			break;
		// Running The Algorithms for all the data sets when argument is 'S'
		case "S":
			if (Files.notExists(Paths.get(FOLDER_NAME), LinkOption.NOFOLLOW_LINKS)) {
				System.out.println("Please use 'D' for execution to create the DataSets before processing");
				break;
			}
			try {
				outputFileWriter = new FileWriter(FOLDER_NAME + outputFileName);
				for (int i = 1; i <= 10; i++) {
					analyser.sort("I", _1000, i);
					analyser.sort("H", _1000, i);
					analyser.sort("Q", _1000, i);
				}
				for (int i = 1; i <= 10; i++) {
					analyser.sort("I", _5000, i);
					analyser.sort("H", _5000, i);
					analyser.sort("Q", _5000, i);
				}
				for (int i = 1; i <= 10; i++) {
					analyser.sort("I", _50000, i);
					analyser.sort("H", _50000, i);
					analyser.sort("Q", _50000, i);
				}

			} catch (IOException e1) {
				System.out.println("IOException has occured!!");
				break;
			} finally {
				try {
					outputFileWriter.close();
				} catch (IOException e) {
					return;
				}
			}
			System.out.println("Output file created as required!!");
			break;
		default:
			System.out.println("Input not identified !!");
			break;
		}
	}

	/**
	 * Function to create the data set. It populates the data elements in different
	 * files with the name convention of "DataSet_{DataSize}_{DataSet}.txt" example:
	 * DataSet_1000_1.txt, DataSet_5000_10.txt
	 */
	private static void createDataSet(final long dataSize, final int setNumber) {
		Random random = new Random();
		File file = new File(FOLDER_NAME + FILE_NAME_PREFIX + dataSize + UNDERSCORE + setNumber + FILE_EXTENSION);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file);
			Set<Integer> numbers = new HashSet<Integer>();
			while (numbers.size() < dataSize) {
				int integer = random.nextInt(1000000);
				if (!numbers.contains(integer)) {
					numbers.add(integer);
				}
			}
			for (Integer integer : numbers) {
				fileWriter.append(String.valueOf(integer));
				fileWriter.append(NEW_LINE);
			}
		} catch (Exception e) {

		} finally {
			try {
				if (fileWriter != null) {
					fileWriter.close();
				}

			} catch (IOException e) {
				System.out.println("IOException!!");
			}
		}
	}

	/**
	 * Calls the Sorting algorithms based on the passed arguments of sortMethod for
	 * the particular data set. This function also calculates the CPU time and the
	 * theoritical Comparisons for each algorithm
	 */
	private void sort(final String sortMethod, long dataSize, int setNumber) throws NumberFormatException, IOException {
		int[] array = getArrayFromFile(dataSize, setNumber);
		long start = 0L;
		long end = 0L;
		int numOfComparisons = 0;
		double theoreticalComparisons = 0D;
		switch (sortMethod) {
		// Calling Insertion Sort
		case "I":
			InsertionSort insertionSort = new InsertionSort();
			start = System.nanoTime();
			numOfComparisons = insertionSort.sort(array);
			end = System.nanoTime();
			theoreticalComparisons = ((dataSize * (dataSize - 1)) / 2);
			break;
		// Calling Heap sort
		case "H":
			HeapSort heapSort = new HeapSort();
			start = System.nanoTime();
			numOfComparisons = heapSort.sort(array);
			end = System.nanoTime();
			theoreticalComparisons = (2 * dataSize * ((double) (Math.log(dataSize) / Math.log(2))));
			break;
		// Calling Quick Sort
		case "Q":
			QuickSort quickSort = new QuickSort();
			start = System.nanoTime();
			numOfComparisons = quickSort.sort(array, 0, array.length - 1, 0);
			end = System.nanoTime();
			theoreticalComparisons = (dataSize * (dataSize - 1)) / 2;
			break;

		default:
			break;
		}
		// Calculation of CPU time
		Long cpuTime = end - start;
		String doubleValue = String.format("%.6f", cpuTime.doubleValue() / 1000000000);

		// Creating output result as per the expected result
		final OutputData data = new OutputData.OutputDataBuilder().sortMethod(sortMethod).setNumber(setNumber)
				.dataSize(dataSize).numOfComparisons(numOfComparisons).cpuTime(doubleValue)
				.theoreticalComparisons(theoreticalComparisons).build();
		this.outputToFile(data);
	}

	/**
	 * Get the array of elements from the files for the given data size and set
	 * number
	 */
	private static int[] getArrayFromFile(final long dataSize, final int setNumber)
			throws NumberFormatException, IOException {
		@SuppressWarnings("resource")
		final BufferedReader br = new BufferedReader(
				new FileReader(FOLDER_NAME + FILE_NAME_PREFIX + dataSize + UNDERSCORE + setNumber + FILE_EXTENSION));
		String line;
		List<Float> listOfIntegers = new ArrayList<>();
		while ((line = br.readLine()) != null) {
			String[] data = line.split(",");
			for (String string : data) {
				listOfIntegers.add(Float.parseFloat(string));
			}
		}
		int[] array = listOfIntegers.stream().mapToInt(i -> i.intValue()).toArray();
		return array;
	}

	/**
	 * Appending result set to 'Output.txt'
	 */
	private void outputToFile(final OutputData data) throws IOException {
		outputFileWriter.append(data.toString());
		outputFileWriter.append(NEW_LINE);
	}

}

// InsertionSort.java
/**
 * Implementation for Insertion Sorting Algorithm
 */
class InsertionSort {

	protected int sort(int[] array) {
		int comparisons = 0;
		for (int i = 0; i < array.length; i++) {
			int key = array[i];
			int j = i - 1;
			while (j >= 0 && key < array[j]) {
				comparisons++;
				array[j + 1] = array[j];
				j--;
			}
			array[j + 1] = key;
		}
		return comparisons;
	}
}

// HeapSort.java
/**
 * Implementation for Heap Sorting Algorithm
 */
class HeapSort {
	private static int arraySize;
	private static int comparisons;

	public int sort(int[] array) {
		comparisons = 0;
		arraySize = array.length;
		// Building heap
		for (int i = arraySize / 2 - 1; i >= 0; i--) {
			buildMaxHeap(array, arraySize, i);
		}

		// Extracting the elements in the heap
		for (int i = arraySize - 1; i >= 0; i--) {
			// Move the root to the end
			swap(array, 0, i);
			// Re build the heap on the reduced heap
			buildMaxHeap(array, i, 0);
		}
		return comparisons;
	}

	public static void buildMaxHeap(int arr[], int arraySize, int i) {
		int left = (2 * i) + 1;
		int right = (2 * i) + 2;
		int max = i;

		if (left < arraySize && arr[left] > arr[max]) {
			max = left;
			comparisons++;
		} else if (left < arraySize) {
			comparisons++;
		}
		if (right < arraySize && arr[right] > arr[max]) {
			max = right;
			comparisons++;
		} else if (right < arraySize) {
			comparisons++;
		}
		// Make the root the largest
		if (max != i) {
			swap(arr, i, max);
			// Re build heap on the sub tree
			buildMaxHeap(arr, arraySize, max);
		}
	}

	// Swap the numbers
	public static void swap(int array[], int i, int j) {
		int tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;
	}

}

// QuickSort.java
/**
 * Implementation for Quick Sorting Algorithm
 */
class QuickSort {

	private static int comparisons = 0;

	protected int sort(int[] array, int lowerIndex, int higherIndex, int actualComparisons) {
		comparisons = actualComparisons;
		int i = lowerIndex;
		int j = higherIndex;
		partition(array, lowerIndex, higherIndex, i, j);
		return comparisons;
	}

	private void partition(int[] array, int lowerIndex, int higherIndex, int i, int j) {
		// Calculate the pivot value from the middle
		int pivot = array[lowerIndex + (higherIndex - lowerIndex) / 2];
		// Divide the array into two
		while (i <= j) {
			comparisons++;
			while (array[i] < pivot) {
				i++;
			}
			comparisons++;
			while (array[j] > pivot) {
				j--;
			}
			if (i <= j) {
				swap(array, i, j);
				i++;
				j--;
			}
		}
		// Call quick sort recursively
		if (lowerIndex < j) {
			sort(array, lowerIndex, j, comparisons);
		}
		if (i < higherIndex) {
			sort(array, i, higherIndex, comparisons);
		}
	}

	// Exchange the numbers
	private void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}

// OutputData.java
/**
 * Output Result for the algorithms are populated in this object using Builder
 * Pattern
 */
class OutputData {

	private String sortMethod;
	private String setNumber;
	private String dataSize;
	private String numOfComparisons;
	private String cpuTime;
	private String theoreticalComparisons;

	public OutputData(OutputDataBuilder builder) {
		this.sortMethod = builder.sortMethod;
		this.setNumber = builder.setNumber;
		this.dataSize = builder.dataSize;
		this.numOfComparisons = builder.numOfComparisons;
		this.cpuTime = builder.cpuTime;
		this.theoreticalComparisons = builder.theoreticalComparisons;
	}

	public String getSortMethod() {
		return sortMethod;
	}

	public String getSetNumber() {
		return setNumber;
	}

	public String getDataSize() {
		return dataSize;
	}

	public String getNumOfComparisons() {
		return numOfComparisons;
	}

	public String getCpuTime() {
		return cpuTime;
	}

	public String getTheoreticalComparisons() {
		return theoreticalComparisons;
	}

	public static class OutputDataBuilder {

		private String sortMethod;
		private String setNumber;
		private String dataSize;
		private String numOfComparisons;
		private String cpuTime;
		private String theoreticalComparisons;

		public OutputDataBuilder sortMethod(final String sortMethod) {
			this.sortMethod = sortMethod;
			return this;
		}

		public OutputDataBuilder setNumber(final int setNumber) {
			this.setNumber = String.valueOf(setNumber);
			return this;
		}

		public OutputDataBuilder dataSize(final long dataSize2) {
			this.dataSize = String.valueOf(dataSize2);
			return this;
		}

		public OutputDataBuilder numOfComparisons(final int numOfComparisons) {
			this.numOfComparisons = String.valueOf(numOfComparisons);
			return this;
		}

		public OutputDataBuilder cpuTime(final String format) {
			this.cpuTime = format;
			return this;
		}

		public OutputDataBuilder theoreticalComparisons(double theoreticalComparisons) {
			this.theoreticalComparisons = String.format("%.2f", theoreticalComparisons);
			return this;
		}

		OutputData build() {
			return new OutputData(this);
		}
	}

	@Override
	public String toString() {
		return new String(this.sortMethod + "," + this.setNumber + "," + this.dataSize + "," + this.numOfComparisons
				+ "," + this.cpuTime + "," + this.theoreticalComparisons);
	}
}
